Gitlab runner role
=========

Installs and configures Gitlab runner server Debian/Ubuntu servers.

Example Playbook
----------------
Docker executor with Docker-in-Docker
```yml
- hosts: all
  become: true
  roles:
    - role: tyumentsev4.gitlab_runner
      vars:
        gitlab_runner_list:
          - name: runner44
            url: https://gitlab.com/
            token: glrt-token
            executor: docker
            state: present
            docker:
              image: docker:24-cli
              privileged: true
              volumes:
                - /certs/client
```
